const jwt = require("jsonwebtoken");
// used in algorithm for encrypting our data which makes it difficult to decode the information without the defined secret keyword
const secret = "NoIdea";


/*
    JSONWEBTOKEN
    - JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
    - Information is kept secure through the use of the secret code
    - Only the system that knows the secret code can decode the encrypted information
*/
module.exports.createAccessToken = (user) => {
    // The data will be received from the registration form/ database
    // When the user logs in, a token will be created with user's information
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    };

    // Generate a JSON web token using the jwt's sign method
    // Generates the token using the form data and the secret code with no additional options provided
    return jwt.sign(data, secret, {});
};


// Token Verification
module.exports.verify = ( req, res, next ) => {
    // the token is retrieved from the request header
    // POSTMAN - authorization - bearer token
    let token = req.headers.authorization

    if (typeof token !== "undefined"){
        token = token.slice (7, token.length);

        console.log(token)
        // the token sent is a type of "bearer" token which when received, contains the "bearer" as a prefix to the string
        return jwt.verify (token, secret, (err, data) => {
            // verify(); validates the token decrypting the token using the secret code

            // should the jwt is invalud
            if (err){
                return res.send ({auth: "failed"});
            } else {
                next ();
                // allows the application to proceed with the next middleware function/callback function in the route
            }
        })
    } else {
        return res.send ({auth: "failed"});
    }
}

// Token decryption

module.exports.decode = (token) => {
    if (token !== "undefined") {
        token = token.slice (7, token.length);
        return jwt.verify (token, secret, (err, data) =>{
            if (err) {
                return null; 
            } else {
                // "decode" method is used to obtain information from theJWT
                // the "token" argument as the one to be decoded
                // {complete: true} - argument option that allows us to return the additional information from the JWT
                // .payload property contains the information provided in the "createAccessToken" method defined above (values for id, email, isAdmin)
                return jwt.decode (token, {complete : true}).payload; 
            }
        })
    } else {
        // if the token does not exist
        return null;
    }
}