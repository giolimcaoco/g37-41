const User = require ("../models/User.js");
const Course = require ("../models/Course.js");
const auth = require ("../auth.js");
const bcrypt = require("bcrypt");


/*
	1. use mongoose method "find" to find duplicate emails
	2. use .then method to send a response based on the result of the find method
*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find( { email: reqBody.email } ).then( result => {
		if ( result.length > 0) { 
			return true; // if there is an existing duplicate email
		} else {
			return false // if the user email is not yet registered in our database
		}
	})
}

// user registration
/*
	1. create a new user object the mongoose model and the information from the request body
	2. make sure that the password is encrypted
	2. save the new user to the DB
*/
module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName, 
		lastName: reqBody.lastName, 
		email: reqBody.email, 
		mobileNo: reqBody.mobileNo, 
		password: bcrypt.hashSync(reqBody.password, 10)
		// hashSync - bcrypt method for encrypting the password of the user once they have successfully registered in our database
		/*
			first parameter, the value to which the encryption will be done - password coming from the request body
			"10", it dictates how many "salt" rounds are to be given to encrypt the value
		*/
	})
	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
}

// User login
/*
	check the DB of the user email exists
	compare the password provided in the request body with the password stored in the database
	generate/return a JSON web token if the user had successfully logged in and return false if not
*/

module.exports.loginUser = ( reqBody ) => {
	return User.findOne( { email: reqBody.email } ).then(result =>{
		// if the user email does not exist
		if(result === null){
			return false;
		// if the user email exists in the database
		} else {
			// compareSync = decodes the encrypted password from the database and compares it to the password received from the request body
			// it's a good that if the value returned by a method/function is boolean, the variable name should be answerable by yes/no
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }
			}else{
				return false;
			}
		}
	} )
} 


//  ------------------------------ACTIVITY

/*Create a /details route that will accept the user’s Id to retrieve the details of a user.
Create a getProfile controller method for retrieving the details of the user:
Find the document in the database using the user's ID
Reassign the password of the returned document to an empty string
Return the result back to the frontend
Process a POST request at the /details route using postman to retrieve the details of the user.
Create a git repository named S37-41.
Add another remote link and push to git with the commit message of Add activity code - S38.
Add the link in Boodle.*/

//  ---------------------------------------

/*module.exports.getProfile = (Info) => {
	return User.findOne (Info.userId).then(result => {
		result.password = "";
		return result
	})
}*/

// -------------------------------------------

module.exports.getProfile = (Info) => {
	return User.findById (Info.userId).then(result => {
		result.password = "";
		return result
	})
}

// -----------------------------------------------


// enroll a user to a class
/*
	to find the document in the database using the users ID
	add the course ID to the users enrollments array
	finally, update the document in the MongoDB Atlas DB
*/
// async await will be used in enrolling since we have two documents to be updated in our DB: user document and course document
module.exports.enroll = async (data) => {
	// adding the courseId in the enrollments of the user
	// returns boolean depending if the updating of the document is successful (true) or failed (false)
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId: data.courseId});
		// saves the updated user information in the DB
		return user.save().then((user, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		})
	})
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// adding userId in the course enrollees array
		course.enrollees.push({userId: data.userId});
		// saves the updated course information in the DB
		return course.save().then((course, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		})
	}) 
	// conditions that 
	if (isUserUpdated && isCourseUpdated) {
		// user enrollment successful
		return true; 
	} else {
		// user enrollment failed
		return false;
	}
}
// ------------------------- ACTIVITY S41 ^^

/*module.exports.enroll = async (data, userData) => {
	
	let isUserUpdated = await User.findById(userData.userId).then(user => {
		user.enrollments.push({courseId: data.courseId});
		return user.save().then((user, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		})
	})
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userData: data.userData});
		return course.save().then((course, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		})
	}) 
	if (isUserUpdated && isCourseUpdated) {
		return true; 
	} else {
		return false;
	}
}*/