const Course = require("../models/Course.js");
const auth = require("../auth.js")

//------mini + daily Activity

/*
1. Refactor the course route to implement user authentication for the admin when creating a course.
2. Refactor the addCourse controller method to implement admin authentication for creating a course.
3. Push to git with the commit message of Add activity code - S39.
4. Add the link in Boodle.
*/


/*module.exports.addCourse = (reqBody, userInfo) => {
 			let newCourse = new Course ({
 				name: reqBody.course.name, 
 				description: reqBody.course.description, 
 				price: reqBody.course.price
 			})
 			return newCourse.save().then((course, error) => {
 				if (error) {
 					return false; 
 				} else {
 					console.log(reqBody)
 					return "New Course Added"
 				}
 			})
 		}*/

// from discussion solution
 module.exports.addCourse = (data) => {
 		let newCourse = new Course({
 			name : data.name,
 			description : data.description,
 			price : data.price
 		});
 		return newCourse.save().then((course, error) => {
 			if (error) {
 				return false;
 			} else {
 				return true;
 			};
 		});
 };


 // retrieve all courses

 module.exports.getAllCourses=()=>{
 	return Course.find({}).then(result => {
 		return result
 	})
 }

 // retrieve all active only

 module.exports.getAllActive=()=>{
 		return Course.find({isActive:true}).then(result=>{
 			return result
 	})
 }

// retrieve a course

module.exports.getCourse=(reqParams)=>{
		return Course.findById(reqParams.courseId).then(result=>{
			return result
	})
}

// update a course

/*
	mini ACT
	create a varialble updateCourse which will contain the information from the request body
	find and update the course using the courseID found in the request params and the variable "updateCourse" containing the information fron the requestBody (findbyIdAndUpdate)
	return the false if there errors, true if the updating is successful
*/

module.exports.updateCourse=(reqParams, reqBody)=>{
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	} 
// findByIdAndUpdate - its purpose is to find a specific ID in the DB (first parameter) and update it using the information from the request body (second parameter)
/*
					SYNTAX

						findByIdAndUpdate (documentId, updatesToBeApplied)
*/
		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error)=>{
			if (error) {
				return false
			}else{
				return result
			}
	})
}

// -------------------------------ACTIVITY S40

/*module.exports.archivingCourse = (reqParams, reqBody) => {
	let archiveCourse = {
		isActive: reqBody.isActive
	}
	return Course.findByIdAndUpdate(reqParams.courseId, archiveCourse).then((result, error)=>{
			if (error) {
				return false
			}else{
				return true
			}
	})
}*/

module.exports.archivingCourse = (reqParams) => {
    let updateActiveField = { isActive: false }
    return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((result, error)=>{
        if(error){
            return false
        } else {
            return true
        }
    })
}



// ------------------------------------------



