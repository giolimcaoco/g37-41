const express = require("express");
const router = express.Router();

const courseController = require("../controllers/courseControllers.js")
const auth = require("../auth.js")

// route for creating a course
/*
1. Refactor the course route to implement user authentication for the admin when creating a course.
2. Refactor the addCourse controller method to implement admin authentication for creating a course.
3. Push to git with the commit message of Add activity code - S39.
4. Add the link in Boodle.
*/

/*router.post("/", auth.verify, (req, res ) =>{
	const userData = auth.decode(req.headers.authorization)
	courseController.addCourse (req.body, {userId: userData.id}).then(resultFromController => res.send (resultFromController));
})*/

// from discussion solution
router.post("/", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send(false);
	} else {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
	}
});



// route for getting all courses

router.get("/all", (req,res)=>{
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

// route for getting ACTIVE users

router.get("/", (req,res)=>{
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
})

router.get("/:courseId", (req,res)=>{
	console.log(req.params.courseId)
	courseController.getCourse(req.params).then(resultFromController =>res.send(resultFromController))
})

router.put ("/:courseId", auth.verify, (req, res)=>{
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send (resultFromController))
})


// Archiving
/*
	ACTIVITY S40
	1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
	2. Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.
	3. Process a PUT request at the /courseId/archive route using postman to archive a course
	5. Push the updates to git with the commit message of Add activity code - S40.
	6. Add the link in Boodle.

*/

/*router.put("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archivingCourse(req.params, req.body).then(resultFromController => res.send (resultFromController))
})*/

router.put("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archivingCourse(req.params).then(resultFromController => res.send (resultFromController))
})


module.exports = router;

