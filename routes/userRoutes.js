const express = require("express");
const router = express.Router();

const userController = require("../controllers/userControllers.js")
const auth = require("../auth.js")

// route for checking if the users email already exist in the DB
// since we will pass a "body" from the request object, post method will be used as HTTP method even if the goal is to just check the DB if there is a user email saved in our DB
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
})

// route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// route for user authentication

router.post("/login", ( req, res ) => {
	userController.loginUser( req.body ).then( resultFromController => res.send( resultFromController ) );
} );


/*  -----------------ACTIVITY------------*/

/*router.post("/details", (req, res) => {
	userController.getProfile (req.body).then( resultFromController => res.send (resultFromController))
})*/


// --------------------------------------

router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile ({userId: userData.id}).then( resultFromController => res.send (resultFromController))
})

// route for user enrollment

router.post("/enroll", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	let data = {
		userId: userData.id,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(resultFromController => res.send (resultFromController))
})


/*
1. Refactor the user route to implement user authentication for the enroll route.
2. Process a POST request at the /enroll route using postman to enroll a user to a course.
3. Push to git with the commit message of Add activity code - S41.
4. Add the link in Boodle.
*/


module.exports = router;