const express = require("express");
const mongoose = require("mongoose");
// allows us to control the apps cross origin resource sharing settings
const cors = require("cors");

// importing routes
const userRoutes = require("./routes/userRoutes.js")
const courseRoutes = require("./routes/courseRoutes.js")

const app = express();

mongoose.connect("mongodb+srv://giolimcaoco:Yx6WOVh2BqmAfb7y@wdc028-course-booking.bczl9fp.mongodb.net/B190-Course-Booking?retryWrites=true&w=majority",
	{
		useNewUrlParser: true, 
		useUnifiedTopology: true 
	}
);

let db = mongoose.connection;					
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the database"));

app.use(cors()); // allows all resources to access our backend application
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes)
app.use("/courses", courseRoutes);

app.listen(process.env.PORT || 4000, () => (console.log(`API now online at port ${process.env.PORT || 4000}`)))
// process.env.PORT handles the environment of the hosting websites should app be hosted in a website such as heroku


/*
	git bash

	npm install cors bcrypt jsonwebtoken
*/
/*
	Heroku deployment
		Procfile
			needed file in heroku
*/